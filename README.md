# pre-commit hooks for Conventional Commits

This hook checks that your commit messages follow the
[Conventional Commits](https://www.conventionalcommits.org/) convention.

## Usage with [pre-commit](https://pre-commit.com/)

This hook only works with the `commit-msg` stage, which is not installed
by default. So you have to install it with `pre-commit install -t commit-msg`
or define the [default_install_hook_types](https://pre-commit.com/#top_level-default_install_hook_types)
setting (recommended) in your `.pre-commit-config.yaml` file.

Example `.pre-commit-config.yaml`:

```yaml
default_install_hook_types:
- pre-commit
- commit-msg
# add other hook types as needed

repos:
- repo: https://gitlab.com/adirelle/pre-commit-hooks-for-conventional-commits.git
  rev: v0.0.1
  hooks:
  - id: conventional-commit-check
```
