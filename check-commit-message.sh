#!/usr/bin/env bash
set -euo pipefail

exec <"$1"

read FIRST_LINE

if ! echo "$FIRST_LINE" | grep -qE '^(build|chore|ci|docs|feat|fix|perf|refactor|revert|style|test)(\([^\)]+\))?!?: '; then
    cat <<EOF >&2
Your commit message must follow the "Conventional Commits" convention.
See https://www.conventionalcommits.org/.

Allowed first line templates:
  type: message
  type!: message
  type(scope): message
  type(scope)!: message

Allowed types: build, chore, ci, docs, feat, fix, perf, refactor, revert, style, test

Your first line: `$FIRST_LINE`
EOF

    exit 1
fi

if read SECOND_LINE; then
    if [[ -n "$SECOND_LINE" ]]; then
        cat <<EOF >&2
Your commit message must follow the "Conventional Commits" convention.
See https://www.conventionalcommits.org/.

The second line of the commit message must be empty.

Your second line: \`$SECOND_LINE\`
EOF
        exit 1
    fi
fi

exit 0
