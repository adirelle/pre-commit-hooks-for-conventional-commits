#!/usr/bin/env bash
set -euo pipefail

THERE="$(dirname "$(readlink -f "$0")")"

FAILED=0

for TEST in "$THERE"/*/[0-9][0-9]-*; do
    echo "========== $(basename $TEST) ==========" >&2
    case "$TEST" in
        */success/*)
            if "$THERE/../check-commit-message.sh" "$TEST"; then
                echo "ok" >&2
            else
                FAILED=1
                echo "FAIL" >&2
            fi
            ;;
        */failure/*)
            if ! "$THERE/../check-commit-message.sh" "$TEST"; then
                echo "ok" >&2
            else
                FAILED=1
                echo "FAIL" >&2
            fi
            ;;
        *)
            echo "unknown test type: $TEST" >&2
            ;;
    esac
done

exit "$FAILED"
